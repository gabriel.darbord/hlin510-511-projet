<?php
class Requete {
	private $dbh;        // DataBase Handler
	private $sth;        // STatement Handler
	private $query_type; // SELECT, UPDATE ou INSERT
	private $logger;


	// constructeur
	public function __construct($params, $logger) {
		$this->logger = $logger;
		$this->log_event('Constructing request...');

		// quelques erreurs
		if(!$params) {
			$this->error('Missing parameters!');
		}
		if(!$params['type']) {
			$this->error('No query type selected!');
		}
		if($params['type'] != 'call' && !$params['table']) {
			$this->error('Missing table parameter!');
		}
		if($params['type'] == 'call' && !$params['procedure']) {
			$this->error('Missing procedure name!');
		}

		$this->query_type = $params['type'];
		$this->dbh = $this->connect();
		$this->sth = $this->params_to_statement($params);

		$this->log_event('Construction successful.');
	}

	// exécution de la requête
	public function execute() {
		$this->log_event('Executing...');

		try{
			$this->sth->execute();
		}catch(PDOException $e){
			$this->error('Execution failed: '.$e->getMessage());
		}

		$this->log_event('Query executed, '.$this->sth->rowCount().' rows '.$this->query_type.'ed.');
	}

	// retour du script
	public function send_result() {
		$this->log_event('Sending results...');
		$res = array('error' => 0);

		if($this->query_type == 'select' || $this->query_type == 'call') {
			try{
				$res['result'] = $this->sth->fetchAll();
			}catch(PDOException $e){
				$this->error('Fetch failed: '.$e->getMessage());
			}
		}

		if(empty($res['result'])) {
			$this->warning('Result is empty.');
		}

		$json = json_encode($res);

		if(!$json) {
			$this->error(json_last_error_msg());
		}

		$this->logger->dump($json, 'resultat');

		echo $json;
		$this->log_event('Sent successfully!');
		exit();
	}
	

	// connexion a la BDD
	private function connect() {
		/* FAC */
		$host = 'mysql.etu.umontpellier.fr';
		$db = 'e20170005330';
		$user = 'e20170005330';
		$pass = 'password'; //TODO à extraire d'un fichier
		
		/* WAMP
		$host = 'localhost:3308';
		$db = 'hlin511-projet';
		$user = 'root';
		$pass = '';
		*/
		$charset = 'utf8';

		$dsn = 'mysql:host='.$host.'; dbname='.$db.'; charset='.$charset;
		$options = [
			// toutes les erreurs lèvent une exception
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			// fetch renvoie un tableau associatif
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			// ne pas émuler la requête
			PDO::ATTR_EMULATE_PREPARES   => false,
			// utiliser le charset utf-8
			PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
		];

		try {
			$res = new PDO($dsn, $user, $pass, $options);
		} catch(PDOException $e) {
			$this->error($e->getMessage());
		}

		$this->log_event("Logged into DBMS successfully.");
		return $res;
	}

	// empêche les injections SQL
	private function bind_params($str, $params) {
		$this->log_event('Initializing statement: '.$str);

		try{
			$stmt = $this->dbh->prepare($str);
		}catch(PDOException $e){
			$this->error('Failed to prepare statement: '.$e->getMessage());
		}

		foreach ($params as $key => $value) {
			$this->log_event('Binding key `'.$key.'` to value `'.($value === null ? 'null' : $value).'`');

			$res = $stmt->bindValue(':'.$key, $value);

			if(!$res){
				$this->error('Failed to bind key `'.$key.'` to value `'.$value.'.');
			}
		}

		return $stmt;
	}


	// prends un tableau de paramètres et retourne la requête correspondante
	private function params_to_statement($params) {
		$this->log_event('Converting params to `'.$this->query_type.'` statement...');

		switch($this->query_type) {
		case 'select':
			$str = 'SELECT * FROM '.$params['table'];

			if(isset($params['where'])) {
				$str .= ' WHERE ';

				$i = 0;
				$len = count($params['where']);
				foreach ($params['where'] as $key => $value) {
					$str .= $key.' = :'.$key;
					if($i < $len - 1) {
						$str .= ' AND ';
					}
					$i++;
				}
			} else {
				$params['where'] = array();
			}

			$str .= ';';
			$stmt = $this->bind_params($str, $params['where']);
			break;

		case 'insert':
			if(!isset($params['values'])) {
				$this->error('No values to insert.');
			}

			$str = 'INSERT INTO '.$params['table'].'(';
			$bind = '';

			$i = 0;
			$len = count($params['values']);
			foreach ($params['values'] as $key => $value) {
				$str .= $key;
				$bind .= ':'.$key;
				if($i < $len - 1) {
					$str .= ', ';
					$bind .= ', ';
				}
				$i++;
			}

			$str .= ') VALUES ('.$bind.');';
			$stmt = $this->bind_params($str, $params['values']);
			break;

		case 'update':
			$str = 'UPDATE '.$params['table'].' SET ';

			$i = 0;
			$len = count($params['values']);
			foreach ($params['values'] as $key => $value) {
				$str .= $key.' = :'.$key;
				if($i < $len - 1) {
					$str .= ', ';
				}
				$i++;
			}

			$where = array();
			if(isset($params['where'])) {
				$str .= ' WHERE ';
			}

			$i = 0;
			foreach ($params['where'] as $key => $value) {
				$where['w'.$key] = $value;
				$str .= $key.' = :'.end($where);
				if($i < $len - 1) {
					$str .= ' AND ';
				}
				$i++;
			}

			$str .= ';';
			$bind = array_merge($params['values'], $where);
			$stmt = $this->bind_params($str, $bind);
			break;

		case 'call':
			$str = 'CALL '.$params['procedure'].'(';

			if(!isset($params['parameters'])) {
				$params['parameters'] = array();
			}

			$i = 0;
			$len = count($params['parameters']);
			foreach ($params['parameters'] as $key => $value) {
				$str .= ':'.$key;
				if($i < $len - 1) {
					$str .= ', ';
				}
				$i++;
			}

			$str .= ');';
			$stmt = $this->bind_params($str, $params['parameters']);
			break;

		default:
			$this->error('Statement initialization error: no query type given.');
		}

		$this->log_event('Statement ready.');
		return $stmt;
	}


	// log des erreurs
	public function log_event($message) {
		$this->logger->info($message);
	}

	// en cas d'ambiguité
	private function warning($warn) {
		$this->logger->warning($warn);
	}

	// en cas d'erreur
	private function error($err) {
		$this->logger->error($err);
		echo json_encode(array('error' => $err));
		exit();
	}
}
?>
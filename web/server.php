<?php
// en-têtes
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Content-Type');
header('Content-Type: application/json');

// afficher TOUTES les erreurs, à commenter en production
error_reporting(E_ALL);
ini_set("display_errors", 1);

// includes
include 'Logger.php';
include 'Requete.php';

// récupère l'entrée donnée par le fetch
$input = file_get_contents('php://input');
$params = json_decode($input, true);

// logs
$logger = new Logger();
$logger->dump($params, 'paramètres');

// date du jour
if($params == 'now') {
	$date = date('Y-m-d');
	$hour = date('H:i');
	$now = array('date' => $date, 'hour' => $hour);
	echo json_encode($now);
	$logger->info('Timestamp queried.');
	exit();
}

// requête
$requete = new Requete($params, $logger);
$requete->execute();
$requete->send_result();
?>
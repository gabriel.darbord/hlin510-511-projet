<?php

class Logger {
	private $file;
	private $log;
	private $success;


	public function __construct() {
		$this->success = true;
		$exists = file_exists('logs.html');
		$this->file = fopen('logs.html', 'a');

		if(!$exists) {
			$init = <<<init
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Logs</title>
	<style>
		#menu {
			position: fixed;
			left: 0;
			top: 0;
			width: 100%;
			background-color: white;
			padding: 10px;
			border-bottom: 2px solid;
		}
		#padding {
			left: 0;
			top: 0;
			padding-bottom: 40px;
		}
		.fourth {
			float: left;
			padding-left: 0.5%;
			width: 24.5%
		}
		.warning {
			color: orange;
		}
		.error {
			color: red;
		}
		.log {
			padding: 5px;
			border-bottom: 1px solid;
		}
		.hidden {
			display: none;
		}
		.xdebug-var-dump {
			margin: 0;
		}
		p {
			margin: 0;
		}
	</style>
	<script>
		function go_to_last() {
			window.scrollTo(0, document.body.scrollHeight);
		}
		function update_nb_log() {
			let logs = document.getElementsByClassName('log');
			let hidden = document.querySelectorAll('body > .hidden');
			let nb_log = logs.length - hidden.length;
			let display = document.getElementById('nb_log');
			display.innerHTML = nb_log;
		}
		function switch_visibility(classname, show) {
			let elements = document.getElementsByClassName(classname);
			for(let e of elements) {
				if(show) {
					e.classList.remove('hidden');
				} else {
					e.classList.add('hidden');
				}
			}
		}
	</script>
</head>
<body>
	<div id="menu">
		<div class="fourth">
			Show:
			<label><input id="show_success" type="checkbox" checked> success</label>
			<label><input id="show_failure" type="checkbox" checked> failure</label>
		</div>
		<div class="fourth">
			Show lines:
			<label><input id="show_info" type="checkbox" checked> infos</label>
			<label><input id="show_warning" type="checkbox" checked> warnings</label>
			<label><input id="show_error" type="checkbox" checked> errors</label>
		</div>
		<div class="fourth">
			Number of logs shown: <span id="nb_log"></span>
		</div>
		<div class="fourth">
			<input type="button" onclick="go_to_last();" value="Go to last">
		</div>
	</div>
	<div id="padding"></div>
	<script>
		let show_success = document.getElementById('show_success');
		show_success.addEventListener('change', function() {
			switch_visibility('success', this.checked);
			update_nb_log();
		});
		let show_failure = document.getElementById('show_failure');
		show_failure.addEventListener('change', function() {
			switch_visibility('failure', this.checked);
			update_nb_log();
		});
		let show_info = document.getElementById('show_info');
		show_info.addEventListener('change', function() {
			switch_visibility('info', this.checked);
		});
		show_warning = document.getElementById('show_warning');
		show_warning.addEventListener('change', function() {
			switch_visibility('warning', this.checked);
		});
		show_error = document.getElementById('show_error');
		show_error.addEventListener('change', function() {
			switch_visibility('error', this.checked);
		});
		document.addEventListener('DOMContentLoaded', function(event) {
			update_nb_log();
		});
	</script>
</body>
</html>
init;

			fwrite($this->file, $init);
		}

		$timestamp = date("Y-m-d H:i:s");
		$this->log = '<p>'.$timestamp.'</p>';
	}


	public function __destruct(){
		if($this->success) {
			$this->log = '<div class="log success">'.$this->log.'</div>';
		} else {
			$this->log = '<div class="log failure">'.$this->log.'</div>';
		}

		fwrite($this->file, $this->log);
		fclose($this->file);
	}


	public function info($message) {
		$this->log .= '<p class="info">'.$message.'</p>';
	}

	public function warning($message) {
		$this->log .= '<p class="warning">#WARN '.$message.'</p>';
	}

	public function error($message) {
		$this->success = false;
		$this->log .= '<p class="error">###ERROR '.$message.'</p>';
	}


	public function dump($var, $varname = '') {
		$this->log .= '<div class="info">';
		if(!empty($varname)) {
			$this->log .= '<span>Dumping `'.$varname.'`: </span>';
		}
		ob_start();
		var_dump($var);
		$dump = ob_get_clean();
		$this->log .= $dump.'</div>';
	}
}

?>
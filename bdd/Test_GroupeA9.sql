-- AUTEURS --

/*
DARBORD Gabriel - 21708126
LAURENT Andréas - 21601010

Nom du groupe : A9
*/


-- CHECKS --

SELECT 'Début des tests des checks' AS 'INFO';

-- saisie d'un evènement avec une durée négative (début après fin)
INSERT INTO EVENEMENT(titre, description, contact, public, debut, fin, theme, LID) VALUES (
	'Les paillotes du JAM', 'Du 16 Mai au 07 Juin, tous les jeudis et vendredis, dès 19h : 3 Concerts par soir, 2 Bars avec Tapas/Assiettes et Expos !', '04 67 58 30 30', 'Tous publics',
	'2019-07-16',
	'2019-06-07',
	'Concert', recuperation_LID('100 Rue Ferdinand de Lesseps', '34070')
);

-- saisir d'un évènement avec un effectif minimum supérieur à l'effectif maximum
INSERT INTO EVENEMENT(titre, description, contact, public, debut, fin, theme, effectif_min, effectif_max, LID) VALUES (
	'Les paillotes du JAM', 'Du 16 Mai au 07 Juin, tous les jeudis et vendredis, dès 19h : 3 Concerts par soir, 2 Bars avec Tapas/Assiettes et Expos !', '04 67 58 30 30', 'Tous publics', '2019-07-16', '2019-08-07',
	'100',
	'0',
	'Concert', recuperation_LID('100 Rue Ferdinand de Lesseps', '34070')
);


-- TRIGGERS --

SELECT 'Début des tests des triggers' AS 'INFO';

-- saisie d'un même évènement 2 fois
INSERT INTO LIEU(nom, adresse, code_postal, ville, acces_handicape) VALUES ('Sud de France ARENA', 'Parc des Expositions', '34000', 'Montpellier', true);

-- saisie d'un évènement n'étant pas dans l'hérault
INSERT INTO LIEU(nom, adresse, code_postal, ville, acces_handicape) VALUES ('Arène Kolizeum', '[-13,-29]', '20000', 'Cania', true);

-- création d'un utilisateur avec un pseudo déja utilisé
INSERT INTO UTILISATEUR(role, pseudonyme, mot_de_passe, contact) VALUES('Contributeur', 'Miu', MD5('cheese.png'), "0799664231");

-- création d'un utilisateur contributeur sans renseigner le champ "contact"
INSERT INTO UTILISATEUR(role, pseudonyme, mot_de_passe) VALUES('Contributeur', 'Chap', MD5('alde'));

-- inscription à un évènement terminé
INSERT INTO INSCRIPTION(EID, UID) VALUES ('1', '1');

-- inscription à un évènement dont l'effectif maximal est atteint
INSERT INTO INSCRIPTION(EID, UID) VALUES ('5', '4');

-- notation d'un évènement par un utilisateur pas inscrit
INSERT INTO NOTE(EID, UID, note) VALUES ('2', '2', '10');

-- notation d'un évènement n'ayant pas commencé par un utilisateur y étant inscrit
INSERT INTO NOTE(EID, UID, note) VALUES ('4', '2', '10');

-- commentaire d'un évènement par un utilisateur n'y étant pas inscrit
INSERT INTO COMMENTAIRE(EID, UID, contenu) VALUES ('2', '2', 'J\'ai pas aimé');

-- authentification erronée
CALL authentification('Miu', MD5('mauvais mdp'));


-- PROCÉDURES D'AFFICHAGE --

SELECT 'Début des tests des procédures d\'affichage' AS 'INFO';

-- affichage de tous les thèmes
CALL available_themes();

-- affichage des sous themes du theme
CALL themes_associes('Sportif');

-- affichage des évènements selon des critères
CALL events_by_criteria(NULL, CURRENT_TIMESTAMP, 'Culturel', NULL, '5', '0');

-- affichage de tous les evenements auxquels est inscrit l'utilisateur
CALL events_for_user('Chiaki');

-- affichage de tous les utilisateurs inscrit à l'évènement
CALL users_for_event('5');

-- affichage de tous les commentaires que l'utilisateur a posté
CALL comments_from_user('Miu');

-- affichage de tous les commentaires que l'évènement a reçu
CALL comments_for_event('5');

-- procédure d'authentification mais qui fonctionne
CALL authentification('Miu', MD5('cheese'));

-- affichage de la note moyenne et du nombre de notations d'un évènement
CALL note_for_event('5');

-- affichage de toutes les notes atribuées par l'utilisateur
CALL notes_from_user('Chiaki');


-- PROCÉDURES D'INSERTION --

SELECT 'Début des tests des procédures d\'insertion' AS 'INFO';

-- inscription d'un nouvel utilisateur
CALL inscription('Chap', MD5('alde'), 'Utilisateur', NULL);

-- insertion d'un nouveau lieu
CALL insert_lieu('Communautés de communes du Clermontais', '24 Rue Doyen René Gosse', '34800', 'Clermont-l\'Hérault', FALSE);

-- insertion d'un nouvel évènement
CALL insert_event('DÉMONSTRATION GRATUITE DE MAGIC THE GATHERING', 'De 14h30 à 15h30, au Loedus, démonstration gratuite de Magic The Gathering', '04 99 91 25 40', 'Tous Publics', '2019-12-18 14:30:00', '2019-12-30 15:30:00', 'Esport', NULL, NULL, recuperation_LID('24 Rue Doyen René Gosse', '34800'));

-- insertion d'un nouvel évènement et d'un nouveau lieu
CALL create_event('Yannick et Pierre les flosseurs', 'c\'est vraiment pas le genre d\'evènements que vous voulez voir', 'j\'ai pas le numéro de Pierrick', 'Adultes', '2000-01-01 00:00:00', '2020-01-01 23:59:59', 'Esport', '11', '12', 'Faculté de Sciences de Montpellier', '30 Place Eugène Bataillon', '34095', 'Montpellier', TRUE);

-- inscription à un évènement
CALL inscription_event('6', '5');

-- notation d'un évènement
CALL insert_note('6', '5', '8');

-- commentaire d'un évènement
CALL insert_comment('6', '5', 'Et c\'est parti pour le commentaire');

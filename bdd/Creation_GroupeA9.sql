-- AUTEURS --

/*
DARBORD Gabriel - 21708126
LAURENT Andréas - 21601010

Nom du groupe : A9
*/


-- BASE DE DONNEE --

/* pas de permission sur le serveur MySql de la fac
DROP DATABASE IF EXISTS BDEVENT;
CREATE DATABASE BDEVENT;
USE BDEVENT;
*/


-- TABLES --

DROP TABLE IF EXISTS INSCRIPTION;
DROP TABLE IF EXISTS NOTE;
DROP TABLE IF EXISTS COMMENTAIRE;
DROP TABLE IF EXISTS UTILISATEUR;
DROP TABLE IF EXISTS EVENEMENT;
DROP TABLE IF EXISTS THEME;
DROP TABLE IF EXISTS COORDONNEES;
DROP TABLE IF EXISTS LIEU;
DROP TABLE IF EXISTS ERRORLOG;

SELECT 'Tables dropped' AS 'INFO';

CREATE TABLE THEME (
	nom VARCHAR(50),
	pere VARCHAR(50) DEFAULT NULL,
	CONSTRAINT PK_THEME PRIMARY KEY (nom),
	CONSTRAINT FK_THEME_THEME FOREIGN KEY (pere) REFERENCES THEME(nom)
);

CREATE TABLE LIEU (
	LID INTEGER AUTO_INCREMENT,
	nom TINYTEXT,
	adresse TINYTEXT,
	code_postal CHAR(5),
	ville TINYTEXT,
	acces_handicape BOOLEAN DEFAULT false,
	CONSTRAINT PK_LIEU PRIMARY KEY (LID),
	CONSTRAINT NN_LIEU_nom CHECK (nom IS NOT NULL),
	CONSTRAINT NN_LIEU_adresse CHECK (adresse IS NOT NULL),
	CONSTRAINT NN_LIEU_code_postal CHECK (code_postal IS NOT NULL),
	CONSTRAINT NN_LIEU_ville CHECK (ville IS NOT NULL)
);

CREATE TABLE COORDONNEES (
	LID INTEGER,
	latitude NUMERIC(6,6),
	longitude NUMERIC(6,6),
	CONSTRAINT PK_COORDONNEES PRIMARY KEY (LID),
	CONSTRAINT FK_COORDONNEES_LIEU FOREIGN KEY (LID) REFERENCES LIEU(LID)
);

CREATE TABLE EVENEMENT (
	EID INTEGER AUTO_INCREMENT,
	titre TEXT,
	description TEXT,
	contact TINYTEXT,
	public TINYTEXT,
	debut TIMESTAMP,
	fin TIMESTAMP,
	theme VARCHAR(50),
	effectif_min INTEGER DEFAULT NULL,
	effectif_max INTEGER DEFAULT NULL,
	tarif DECIMAL(5,2) DEFAULT NULL,
	note DECIMAL(2,1) DEFAULT NULL,
	nb_notes INTEGER DEFAULT 0,
	LID INTEGER,
	CONSTRAINT PK_EVENEMENT PRIMARY KEY (EID),
	CONSTRAINT FK_EVENEMENT_THEME FOREIGN KEY (theme) REFERENCES THEME(nom),
	CONSTRAINT FK_EVENEMENT_LIEU FOREIGN KEY (LID) REFERENCES LIEU(LID),
	CONSTRAINT NN_EVENEMENT_titre CHECK (titre IS NOT NULL),
	CONSTRAINT NN_EVENEMENT_public CHECK (public IS NOT NULL),
	CONSTRAINT NN_EVENEMENT_debut CHECK (debut IS NOT NULL),
	CONSTRAINT NN_EVENEMENT_fin CHECK (fin IS NOT NULL),
	CONSTRAINT NN_EVENEMENT_LID CHECK (LID IS NOT NULL),
	CONSTRAINT OK_EVENEMENT_duree CHECK (debut <= fin),
	CONSTRAINT OK_EVENEMENT_effectifs CHECK (effectif_max - effectif_min > 0),
	CONSTRAINT IN_EVENEMENT_public CHECK (public IN ('Tous publics', 'Enfants', 'Adultes'))
);

CREATE TABLE UTILISATEUR (
	UID INTEGER AUTO_INCREMENT,
	role VARCHAR(20),
	pseudonyme VARCHAR(20),
	mot_de_passe BLOB,
	contact TINYTEXT DEFAULT NULL,
	CONSTRAINT PK_UTILISATEUR PRIMARY KEY (UID),
	CONSTRAINT NN_UTILISATEUR_pseudo CHECK (pseudonyme IS NOT NULL),
	CONSTRAINT NN_UTILISATEUR_mdp CHECK (mot_de_passe IS NOT NULL),
	CONSTRAINT IN_UTILISATEUR_role CHECK (role	IN ('Administrateur', 'Contributeur', 'Utilisateur'))
);

CREATE TABLE COMMENTAIRE (
	EID INTEGER,
	UID INTEGER,
	timbre_temporel TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	contenu TEXT,
	CONSTRAINT PK_COMMENTAIRE PRIMARY KEY (UID, EID, timbre_temporel),
	CONSTRAINT FK_COMMENTIARE_UTILISATEUR FOREIGN KEY (UID) REFERENCES UTILISATEUR(UID),
	CONSTRAINT FK_COMMENTIARE_EVENEMENT FOREIGN KEY (EID) REFERENCES EVENEMENT(EID),
	CONSTRAINT NN_COMMENTAIRE_contenu CHECK (contenu IS NOT NULL)
);

CREATE TABLE NOTE (
	EID INTEGER,
	UID INTEGER,
	note INTEGER,
	CONSTRAINT PK_NOTE PRIMARY KEY (UID, EID),
	CONSTRAINT FK_NOTE_UTILISATEUR FOREIGN KEY (UID) REFERENCES UTILISATEUR(UID),
	CONSTRAINT FK_NOTE_EVENEMENT FOREIGN KEY (EID) REFERENCES EVENEMENT(EID),
	CONSTRAINT NN_NOTE_note CHECK (note IS NOT NULL AND note >= 0 AND note <= 10)
);

CREATE TABLE INSCRIPTION (
	EID INTEGER,
	UID INTEGER,
	date_inscription TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	CONSTRAINT PK_INSCRIPTION PRIMARY KEY (EID, UID),
	CONSTRAINT FK_INSCRIPTION_UTILISATEUR FOREIGN KEY (UID) REFERENCES UTILISATEUR(UID),
	CONSTRAINT FK_INSCRIPTION_EVENEMENT FOREIGN KEY (EID) REFERENCES EVENEMENT(EID)
);

CREATE TABLE ERRORLOG (
	ID INTEGER AUTO_INCREMENT,
	message TINYTEXT,
	timbre_temporel TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	CONSTRAINT PK_ERRORLOG PRIMARY KEY (ID)
);

SELECT 'Tables created' AS 'INFO';


-- TRIGGERS --

DROP TRIGGER IF EXISTS verif_insertion_lieu;
DROP TRIGGER IF EXISTS mise_a_jour_note;
DROP TRIGGER IF EXISTS validation_notation;
DROP TRIGGER IF EXISTS inscription_event;
DROP TRIGGER IF EXISTS verif_utilisateur;
DROP TRIGGER IF EXISTS verif_commentaire;
DROP FUNCTION IF EXISTS recuperation_LID;
DROP FUNCTION IF EXISTS recup_effectif_event;
DROP PROCEDURE IF EXISTS available_themes;
DROP PROCEDURE IF EXISTS themes_associes;
DROP PROCEDURE IF EXISTS events_for_user;
DROP PROCEDURE IF EXISTS users_for_event;
DROP PROCEDURE IF EXISTS events_by_criteria;
DROP PROCEDURE IF EXISTS comments_from_user;
DROP PROCEDURE IF EXISTS comments_for_event;
DROP PROCEDURE IF EXISTS note_for_event;
DROP PROCEDURE IF EXISTS notes_from_user;
DROP PROCEDURE IF EXISTS authentification;
DROP PROCEDURE IF EXISTS inscription;
DROP PROCEDURE IF EXISTS insert_event;
DROP PROCEDURE IF EXISTS create_event;
DROP PROCEDURE IF EXISTS inscription_event;
DROP PROCEDURE IF EXISTS insert_lieu;
DROP PROCEDURE IF EXISTS insert_note;
DROP PROCEDURE IF EXISTS insert_comment;

SELECT 'Triggers, functions and procedures dropped' AS 'INFO';

DELIMITER $$

/*
	Récupération de l'ID du lieu en fonction de l'adresse et du code postal
*/
CREATE FUNCTION recuperation_LID(adresse TINYTEXT, CP CHAR(5))
	RETURNS INTEGER
BEGIN
	RETURN(
		SELECT LID
		FROM LIEU
		WHERE LOWER(LIEU.adresse) LIKE LOWER(adresse)
		AND LIEU.code_postal LIKE CP);
END$$

/*
	Récupération du nombre de personnes inscrites à un évènement
*/
CREATE FUNCTION recup_effectif_event(EID INTEGER)
	RETURNS INTEGER
BEGIN
	RETURN(
		SELECT COUNT(*)
		FROM EVENEMENT E
		JOIN INSCRIPTION I ON E.EID = I.EID
		WHERE E.EID = EID);
END$$

/*
	Vérifie que le code postal est bien dans le département et que le lieu n'existe pas déjà
*/
CREATE TRIGGER verif_insertion_lieu
	BEFORE INSERT ON LIEU
	FOR EACH ROW
BEGIN	
	DECLARE ERR TINYTEXT;
	/* code postal valide */
	IF SUBSTR(NEW.code_postal, 1, 2) <> '34'
	THEN
		SET ERR = CONCAT('Code postal \'', CAST(NEW.code_postal AS CHAR), '\' invalide.');
		INSERT INTO ERRORLOG(message) VALUES (ERR);
		SIGNAL SQLSTATE '45000' SET message_text = ERR;
	END IF;
	/* lieu existe déjà */
	IF (NEW.adresse, NEW.code_postal) IN (SELECT adresse, code_postal FROM LIEU)
	THEN
		SET ERR = CONCAT('Le lieu à l\'adresse ', NEW.adresse, ' ', NEW.code_postal, ' existe déjà.');
		INSERT INTO ERRORLOG(message) VALUES (ERR);
		SIGNAL SQLSTATE '45000' SET message_text = ERR;
	END IF;
END$$


/*
	Met à jour automatiquement la note moyenne à chaque notation d'un utilisateur
*/
CREATE TRIGGER mise_a_jour_note
	AFTER INSERT ON NOTE
	FOR EACH ROW
BEGIN
	DECLARE old_note DECIMAL(2,1);
	SELECT note INTO old_note
	FROM EVENEMENT E
	WHERE E.EID = NEW.EID;

	IF (old_note IS NULL)
	THEN
		UPDATE EVENEMENT
		SET note = NEW.note, nb_notes = 1
		WHERE EID = NEW.EID;
	ELSE 
		UPDATE EVENEMENT
		SET note = (note*nb_notes + NEW.note)/(nb_notes+1) , nb_notes = nb_notes + 1
		WHERE EID = NEW.EID;
	END IF;
END$$


/*
	Ne valide une notation que si l'utilisateur est inscrit et que cet évènement est passé
*/
CREATE TRIGGER validation_notation
	BEFORE INSERT ON NOTE
	FOR EACH ROW
BEGIN 
	DECLARE DATE_EV DATE;
	DECLARE ERR TINYTEXT;
	DECLARE PSEUDO TINYTEXT;
	SELECT debut INTO DATE_EV
	FROM EVENEMENT E
	WHERE NEW.EID = E.EID;
	IF (CURRENT_TIMESTAMP < DATE_EV)
	THEN
		SET ERR = CONCAT('Notation impossible : l\'évènement #', NEW.EID, ' n\'a pas commencé.');
		INSERT INTO ERRORLOG(message) VALUES (ERR);
		SIGNAL SQLSTATE '45000' SET message_text = ERR;
	END IF;
	IF (NOT EXISTS (
		SELECT * FROM INSCRIPTION I
		WHERE NEW.UID = I.UID AND NEW.EID = I.EID))
	THEN	
		SELECT pseudonyme INTO PSEUDO
		FROM UTILISATEUR U
		WHERE NEW.UID = U.UID;
		SET ERR = CONCAT('Notation impossible : l\'utilisateur ', PSEUDO, ' n\'est pas inscrit à l\'évènement #', NEW.EID,'.');
		INSERT INTO ERRORLOG(message) VALUES (ERR);
		SIGNAL SQLSTATE '45000' SET message_text = ERR;
	END IF;
END$$

/*
	Vérifie si l'évènement auquel on s'inscrit n'est pas terminé et que l'effectif max n'est pas dépassé
*/
CREATE TRIGGER inscription_event
	BEFORE INSERT ON INSCRIPTION
	FOR EACH ROW
BEGIN
	DECLARE DATE_EV DATE;
	DECLARE ERR TINYTEXT;
	DECLARE EFFMAX INTEGER;

	SELECT fin, effectif_max INTO DATE_EV, EFFMAX
	FROM EVENEMENT E
	WHERE NEW.EID = E.EID;
	IF (CURRENT_TIMESTAMP > DATE_EV)
	THEN
		SET ERR = CONCAT('Inscription impossible : l\'évènement #', NEW.EID, ' est terminé.');
		INSERT INTO ERRORLOG(message) VALUES (ERR);
		SIGNAL SQLSTATE '45000' SET message_text = ERR;
	END IF;
	IF (EFFMAX IS NOT NULL AND recup_effectif_event(NEW.EID) >= EFFMAX )
	THEN
		SET ERR = CONCAT('Inscription impossible : l\'évènement #', NEW.EID, ' n\'accepte plus de nouveaux participants.');
		INSERT INTO ERRORLOG(message) VALUES (ERR);
		SIGNAL SQLSTATE '45000' SET message_text = ERR;
	END IF;
END$$

/*
	Vérifie si le pseudo est déja pris, et qu'un contributeur renseigne son contact
*/
CREATE TRIGGER verif_utilisateur
	BEFORE INSERT ON UTILISATEUR
	FOR EACH ROW
BEGIN
	DECLARE ERR TINYTEXT;
	IF EXISTS(
		SELECT *
		FROM UTILISATEUR U
		WHERE NEW.pseudonyme = U.pseudonyme)
	THEN
		SET ERR = CONCAT('Erreur lors de la création d\'un nouvel utilisateur : le pseudonyme \'', NEW.pseudonyme, '\' n\'est pas disponible.');
		INSERT INTO ERRORLOG(message) VALUES (ERR);
		SIGNAL SQLSTATE '45000' SET message_text = ERR;
	END IF;
	IF(NEW.role LIKE 'Contributeur' AND (NEW.contact IS NULL OR LENGTH(NEW.contact) = 0))
	THEN
		SET ERR = CONCAT('Erreur lors de la création d\'un nouvel utilisateur : les contributeurs se doivent de renseigner le champ "Contact".');
		INSERT INTO ERRORLOG(message) VALUES (ERR);
		SIGNAL SQLSTATE '45000' SET message_text = ERR;
	END IF;
END$$

/*
	Vérifie si un utilisateur est inscrit à un évènement avant de le commenter
*/
CREATE TRIGGER verif_commentaire
	BEFORE INSERT ON COMMENTAIRE
	FOR EACH ROW
BEGIN
	DECLARE ERR TINYTEXT;
	DECLARE PSEUDO VARCHAR(20);
	IF NOT EXISTS(
		SELECT *
		FROM INSCRIPTION I
		WHERE I.UID = NEW.UID AND I.EID = NEW.EID)
	THEN
		SELECT pseudonyme INTO PSEUDO
		FROM UTILISATEUR U
		WHERE NEW.UID = U.UID;
		SET ERR = CONCAT('Erreur lors de la tentative de commentaire : l\'utilisateur \'', PSEUDO,'\' n\'est pas inscrit à l\'évènement #', NEW.EID,'.');
		INSERT INTO ERRORLOG(message) VALUES (ERR);
		SIGNAL SQLSTATE '45000' SET message_text = ERR;
	END IF;
END$$

/*
	Affiche tous les thèmes
*/
CREATE PROCEDURE available_themes()
BEGIN
	SELECT nom FROM THEME;
END$$

/*
	Affiche tous les sous-thèmes du thème passé en paramètre
*/
CREATE PROCEDURE themes_associes(
	IN p_theme VARCHAR(50))
BEGIN
	(SELECT nom
	FROM THEME
	WHERE pere LIKE p_theme
	) UNION (
	SELECT nom
	FROM THEME
	WHERE nom = p_theme);
END$$

/*
	Affiche tous les évènements auxquels s'est inscrit l'utilisateur passé en paramètre
*/
CREATE PROCEDURE events_for_user(
	IN p_pseudo VARCHAR(20))
BEGIN
	DECLARE JUNKO INTEGER;
	SELECT UID INTO JUNKO
	FROM UTILISATEUR
	WHERE pseudonyme LIKE p_pseudo;
	SELECT titre, theme, note, description 
	FROM EVENEMENT E
	JOIN INSCRIPTION I ON I.EID = E.EID
	WHERE I.UID = JUNKO;	
END$$

/*
	Affiche la liste des inscrits à un evènement passé en paramètre
*/
CREATE PROCEDURE users_for_event(
	IN p_EID INTEGER)
BEGIN
	SELECT pseudonyme, role, U.contact
	FROM UTILISATEUR U
	JOIN INSCRIPTION I ON I.UID = U.UID
	JOIN EVENEMENT E ON E.EID = I.EID
	WHERE E.EID = p_EID;
END$$



/*
	Affiche tous les évènements répondant aux critères passés en paramètre
*/
CREATE PROCEDURE events_by_criteria(
	IN p_titre TEXT,
	IN p_date TIMESTAMP,
	IN p_theme VARCHAR(50),
	IN p_public TINYTEXT,
	IN p_note_min DECIMAL(2,1),
	IN p_hide_full INTEGER)
BEGIN
	SELECT *
	FROM EVENEMENT E
	WHERE INSTR(E.titre, IF(p_titre IS NOT NULL, p_titre, '')) > 0
	AND E.fin > p_date
	AND INSTR(E.theme, IF(p_theme IS NOT NULL, p_theme, '')) = 1
	AND INSTR(E.public, IF(p_public IS NOT NULL, p_public, '')) = 1
	AND E.note >= IF(p_note_min IS NOT NULL, p_note_min, 0)
	AND IF(E.effectif_max IS NULL, TRUE, IF(p_hide_full, recup_effectif_event(E.EID) < E.effectif_max, TRUE));
END$$

/*
	Affiche tous les commentaires postés par l'utilisateur courant, triés du plus récent au moins récent
*/
CREATE PROCEDURE comments_from_user(
	IN p_UID INTEGER)
BEGIN
	SELECT C.EID AS event_id, titre, timbre_temporel, contenu
	FROM COMMENTAIRE C
	JOIN UTILISATEUR U ON U.UID = C.UID  
	JOIN EVENEMENT E ON E.EID = C.EID
	WHERE U.UID = p_UID
	ORDER BY timbre_temporel DESC;
END$$

/*
	Affiche tous les commentaires postés pour un évènement, triés du plus récent au moins récent
*/
CREATE PROCEDURE comments_for_event(
	IN p_EID INTEGER)
BEGIN
	SELECT pseudonyme, timbre_temporel, contenu
	FROM COMMENTAIRE C
	JOIN UTILISATEUR U ON U.UID = C.UID
	WHERE C.EID = p_EID;
END$$

/*
	Renvoie une erreur si la combinaison utilisateur/mdp n'est pas dans la BDD, renvoie une table avec le role de l'utilisateur authentifié sinon
*/
CREATE PROCEDURE authentification(
	IN p_pseudo VARCHAR(20),
	IN p_pass BLOB)
BEGIN
	DECLARE ERR TINYTEXT;
	DECLARE role TINYTEXT;
	DECLARE user_id INTEGER;
	DECLARE pseudo VARCHAR(20);
	DECLARE contact TINYTEXT;	
	SELECT U.role, U.contact, U.UID, U.pseudonyme INTO role, contact, user_id, pseudo
	FROM UTILISATEUR U
	WHERE U.pseudonyme LIKE p_pseudo AND U.mot_de_passe = p_pass;
	IF (ROLE IS NULL)
	THEN
		SET ERR = CONCAT('Erreur d\'authentification : les identifiants sont incorrects.');
		INSERT INTO ERRORLOG(message) VALUES (ERR);
		SIGNAL SQLSTATE '45000' SET message_text = ERR;
	ELSE
		SELECT user_id, pseudo, role, contact;
	END IF;
END$$

/*
	Sélection de la note moyenne d'un évènement uniquement
*/
CREATE PROCEDURE note_for_event(
	IN p_EID INTEGER)
BEGIN
	SELECT note, nb_notes
	FROM EVENEMENT E
	WHERE E.EID = p_EID;
END$$

/*
	Sélection de toutes les notes attribuées par un utilisateur
*/
CREATE PROCEDURE notes_from_user(
	IN p_pseudo VARCHAR(20))
BEGIN
	SELECT N.note, titre, description, E.note AS note_moy
	FROM NOTE N
	JOIN EVENEMENT E ON E.EID = N.EID
	JOIN UTILISATEUR U ON U.UID = N.UID
	WHERE U.pseudonyme LIKE p_pseudo;
END$$

/*
	Inscription
*/
CREATE PROCEDURE inscription(
	IN p_pseudo VARCHAR(20),
	IN p_pass BLOB,
	IN p_role TINYTEXT,
	IN p_contact TINYTEXT)
BEGIN
	INSERT INTO UTILISATEUR(pseudonyme, mot_de_passe, role, contact) VALUES (p_pseudo, p_pass, p_role, p_contact);
	SELECT U.UID AS user_id, U.pseudonyme AS pseudo, U.role AS role, U.contact AS contact
	FROM UTILISATEUR U
	WHERE U.pseudonyme LIKE p_pseudo;
END$$

/*
	Insertion d'évènement 
*/
CREATE PROCEDURE insert_event(
	IN p_titre TEXT,
	IN p_description TEXT,
	IN p_contact TINYTEXT,
	IN p_public TINYTEXT,
	IN p_debut TIMESTAMP,
	IN p_fin TIMESTAMP,
	IN p_theme VARCHAR(50),
	IN p_effectif_min INTEGER,
	IN p_effectif_max INTEGER,
	IN p_LID INTEGER)
BEGIN
	INSERT INTO EVENEMENT(titre, description, contact, public, debut, fin, theme, effectif_min, effectif_max, LID) VALUES(p_titre, p_description, p_contact, p_public, p_debut, p_fin, p_theme, p_effectif_min, p_effectif_max, p_LID);
END$$

/*
	Insertion de lieu
*/
CREATE PROCEDURE insert_lieu(
	IN p_nom TINYTEXT,
	IN p_adresse TINYTEXT,
	IN p_code_postal VARCHAR(5),
	IN p_ville TINYTEXT,
	IN p_acces_handicape BOOLEAN)
BEGIN
	INSERT INTO LIEU(nom, adresse, code_postal, ville, acces_handicape) VALUES(p_nom, p_adresse, p_code_postal, p_ville, p_acces_handicape);
END$$

/*
	Créée un évènement en vérifiant si le lieu passé en paramètre existe ou pas, le créant le cas échéant avant de l'insérer
*/
CREATE PROCEDURE create_event(
	IN p_titre TEXT,
	IN p_description TEXT,
	IN p_contact TINYTEXT,
	IN p_public TINYTEXT,
	IN p_debut TIMESTAMP,
	IN p_fin TIMESTAMP,
	IN p_theme VARCHAR(50),
	IN p_effectif_min INTEGER,
	IN p_effectif_max INTEGER,
	IN p_nom TINYTEXT,
	IN p_adresse TINYTEXT,
	IN p_code_postal VARCHAR(5),
	IN p_ville TINYTEXT,
	IN p_acces_handicape BOOLEAN)
BEGIN
	DECLARE p_LID INTEGER;	
	SET p_LID = recuperation_LID(p_adresse, p_code_postal);
	IF(p_LID IS NULL)
	THEN
		CALL insert_lieu(p_nom, p_adresse, p_code_postal, p_ville, p_acces_handicape);
		SET p_LID = recuperation_LID(p_adresse, p_code_postal);
	END IF;
	CALL insert_event(p_titre, p_description, p_contact, p_public, p_debut, p_fin, p_theme, p_effectif_min, p_effectif_max, p_LID);
END$$ 

/*
	Inscription à un évènement
*/
CREATE PROCEDURE inscription_event(
	IN p_EID INTEGER,
	IN p_UID INTEGER)
BEGIN
	INSERT INTO INSCRIPTION(EID, UID) VALUES (p_EID, p_UID);
END$$

/*
	Insertion de note
*/
CREATE PROCEDURE insert_note(
	IN p_EID INTEGER,
	IN p_UID INTEGER,
	IN p_NOTE DECIMAL(2,1))
BEGIN
	INSERT INTO NOTE(EID, UID, note) VALUES (p_EID, p_UID, p_note);
END$$

/*
	Insertion de commentaire
*/
CREATE PROCEDURE insert_comment(
	IN p_EID INTEGER,
	IN p_UID INTEGER,
	IN p_contenu TEXT)
BEGIN
	INSERT INTO COMMENTAIRE(EID, UID, contenu) VALUES (p_EID, P_UID, p_contenu);
END$$

DELIMITER ;

SELECT 'Triggers, functions and procedures created' AS 'INFO';


-- INSERTIONS --

SELECT 'Beginning insertions' AS 'INFO';

INSERT INTO THEME VALUES ('Culturel', NULL);
INSERT INTO THEME VALUES ('Sportif', NULL);
INSERT INTO THEME VALUES ('Fête', 'Culturel');
INSERT INTO THEME VALUES ('Gastronomie', 'Culturel');
INSERT INTO THEME VALUES ('Spectacle', 'Culturel');
INSERT INTO THEME VALUES ('Concert', 'Culturel');
INSERT INTO THEME VALUES ('Exposition', 'Culturel');
INSERT INTO THEME VALUES ('Marathon', 'Sportif');
INSERT INTO THEME VALUES ('Sport nautique', 'Sportif');
INSERT INTO THEME VALUES ('Randonnée', 'Sportif');
INSERT INTO THEME VALUES ('Compétition', 'Sportif');
INSERT INTO THEME VALUES ('Esport', 'Sportif');

SELECT 'Table THEME initialized' AS 'INFO';

INSERT INTO LIEU(nom, adresse, code_postal, ville) VALUES ('JAM', '100 Rue Ferdinand de Lesseps', '34070', 'Montpellier');
INSERT INTO LIEU(nom, adresse, code_postal, ville) VALUES ('Domaine départemental de Bayssan', 'Route de Vendres', '34500', 'Béziers'); 
INSERT INTO LIEU(nom, adresse, code_postal, ville, acces_handicape) VALUES ('Sud de France ARENA', 'Parc des Expositions', '34000', 'Montpellier', true);
INSERT INTO LIEU(nom, adresse, code_postal, ville, acces_handicape) VALUES ('Zaap d\'Astrub', '[5,-18]', '34200', 'Astrub', true);
INSERT INTO LIEU(nom, adresse, code_postal, ville) VALUES ('Maison Départementale de l\'Environnement', 'Domaine Départemental de Restinclieres', '34730', 'Prades-le-Lez');
/*INSERT INTO LIEU(nom, adresse, code_postal, ville) VALUES ();*/

SELECT 'Table LIEU initialized' AS 'INFO';

INSERT INTO EVENEMENT(titre, description, contact, public, debut, fin, theme, LID) VALUES (
	'Les paillotes du JAM', 
	'Du 16 Mai au 07 Juin, tous les jeudis et vendredis, dès 19h : 3 Concerts par soir, 2 Bars avec Tapas/Assiettes et Expos !',
	'04 67 58 30 30',
	'Tous publics',
	'2019-05-16',
	'2019-06-07',
	'Concert',
	recuperation_LID('100 Rue Ferdinand de Lesseps', '34070')
);
INSERT INTO EVENEMENT(titre, description, contact, public, debut, fin, theme, LID) VALUES (
	'Jean_François Zygel improvise sur Beethoven',
	'L\'un des plus grands pianistes français de notre époque. ',
	'+33 4 67 28 37 32',
	'Tous publics',
	'2020-11-10',
	'2020-12-10',
	'Concert',
	recuperation_LID('Route de Vendres', '34500')
);
INSERT INTO EVENEMENT(titre, description, contact, public, debut, fin, theme, LID) VALUES (
	'BIG FLO & OLI - La Vie de Rêve',
	'Bleu Citron et Golden Child présentent « La Vie de Rêve » en tournée Après avoir réuni 60 000 personnes au Stadium de Toulouse et annoncé trois dates exceptionnelles à Paris La Défense Arena (26.10.2019), au Palais 12 de Bruxelles (19.10.2019) et à l’Arena de Genève le (28.09.2019), les deux frères reviennent avec leur nouveau spectacle « La Vie de Rêve » pour une ultime tournée de 09 dates.',
	'0562734477',
	'Tous publics',
	'2020-01-18',
	'2020-01-18',
	'Concert',
	recuperation_LID('Parc des Expositions', '34000')
);
INSERT INTO EVENEMENT(titre, description, contact, public, debut, fin, theme, LID) VALUES (
	'OPEN SUD DE FRANCE',
	'L’Open Sud de France est de retour en 2020 à la Sud de France Arena Ouverture de la billetterie pour les licenciés le 01/10 et pour le Grand Public le 8/10.',
	'billetterie@spl-occitanie-events.com',
	'Tous publics',
	'2020-02-02',
	'2020-02-09',
	'Compétition',
	recuperation_LID('Parc des Expositions', '34000')
);
INSERT INTO EVENEMENT(titre, description, contact, public, debut, fin, effectif_max, theme, LID) VALUES (
	'Ateliers vacances scolaires spécial enfants : DECORATIONS EN ARGILE',
	'Avec l’association « l’Argile en Tête » Mon beau sapin, roi des forets, que j’aiiiimme ta verdure !!! A 2 jours de noël, ton sapin doit déjà briller de mille feux, mais il reste sûrement un peu de place pour rajouter ta touche personnelle ? Alors viens nous rejoindre pour fabriquer tes décorations de noël en argile. Choisi ta couleur, ta forme et puis bien sûr décore les ! Tu pourras même les assembler pour créer une belle décoration murale ! Alors à toi de jouer !',
	'04 67 67 82 20',
	'Enfants',
	'2019-12-18 14:00',
	'2020-02-01 16:30',
	'3',
	'Culturel',
	recuperation_LID('Domaine Départemental de Restinclieres', '34730')
);
/*INSERT INTO EVENEMENT(titre, description, contact, public, debut, fin, theme, LID) VALUES ();*/

SELECT 'Table EVENEMENT initialized' AS 'INFO';

INSERT INTO UTILISATEUR(role, pseudonyme, mot_de_passe) VALUES ('Administrateur', 'Junko', MD5('password1'));
INSERT INTO UTILISATEUR(role, pseudonyme, mot_de_passe, contact) VALUES ('Contributeur', 'Miu', MD5('cheese'), '0645981472');
INSERT INTO UTILISATEUR(role, pseudonyme, mot_de_passe) VALUES ('Utilisateur', 'Chiaki', MD5('gamertime.png'));
INSERT INTO UTILISATEUR(role, pseudonyme, mot_de_passe) VALUEs ('Utilisateur', 'Kyoko', MD5('purpletaste'));

SELECT 'Table UTILISATEUR initialized' AS 'INFO';

INSERT INTO INSCRIPTION(EID, UID) VALUES ('5', '1');
INSERT INTO INSCRIPTION(EID, UID) VALUES ('5', '2');
INSERT INTO INSCRIPTION(EID, UID) VALUES ('5', '3');
INSERT INTO INSCRIPTION(EID, UID) VALUES ('4', '3');
INSERT INTO INSCRIPTION(EID, UID) VALUES ('3', '2');
INSERT INTO INSCRIPTION(EID, UID) VALUES ('3', '4');

SELECT 'Table INSCRIPTION initialized' AS 'INFO';

INSERT INTO NOTE(EID, UID, note) VALUES ('5', '1', '7');
INSERT INTO NOTE(EID, UID, note) VALUES ('5', '2', '10');
INSERT INTO NOTE(EID, UID, note) VALUES ('5', '3', '6');

SELECT 'Table NOTE initialized' AS 'INFO';

INSERT INTO COMMENTAIRE(EID, UID, contenu) VALUES ('5', '1', "Magnifique");
INSERT INTO COMMENTAIRE(EID, UID, contenu) VALUES ('5', '2', "J'ai adoré ! Merci beaucoup pour ce moment de partage");
INSERT INTO COMMENTAIRE(EID, UID, contenu) VALUES ('5', '3', "Mouais pas terrible hein, j'ai vu mieux.");

SELECT 'Table COMMENTAIRE initialized' AS 'INFO';


-- AFFICHAGES --

SELECT * FROM EVENEMENT;
SELECT * FROM LIEU;
SELECT * FROM UTILISATEUR;
SELECT * FROM INSCRIPTION;
SELECT * FROM NOTE;
SELECT * FROM COMMENTAIRE;
SELECT * FROM THEME;
/* tables vides
SELECT * FROM COORDONNEES;
SELECT * FROM ERRORLOG;
*/
